      PROGRAM GROUP
C
C  22.9.88 FORTRAN 77 POUR SUN4
C
C  lecture complete des G : MXJG
C
C  EDITION DE COEFFICIENTS DE COUPLAGE
C
C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'TDS_PARAMETER'
      CHARACTER NOM*40
      CHARACTER KC*2,IIM*2,IUG*2
      COMMON /RL1/ LUSOR,IS(5)
      COMMON /RL2/ KC(5),IIM(3),IUG(2)
C
101   FORMAT(/,'0: Change name of output file.',
     &       /,'1: 3C-sigma coefficients.',
     &       /,'2: 6C coefficients.',
     &       /,'3: G matrix elements.',
     &       /,'4: K isoscalar factors.',
     &       /,'5: Rotational reduced matrix elements.',
     &       /,'6: 3J-m coefficients.',
     &       /,'7: Exit.',
     &      //,'Choose one of the items above:')
102   FORMAT(A)
103   FORMAT
     &  (/,' GROUP Program.',
     &  //,'Please read file group_README for detailed instructions.',
     &   /,'Initializing, please wait.')
104   FORMAT('Loading G : integer ...')
105   FORMAT(/,'Type name of output file (ENTER = screen): ')
C
C-----------------------------------------------------------------------
C
      WRITE(*,103)
      WRITE(*,104)
      CALL LECTG(MXJG)
      CALL FACTO
      CALL CAL6C
C
C-----------------------------------------------------------------------
C
11    WRITE(*,105)
      READ(*,102) NOM
      IF(NOM.EQ.' ') THEN
       LUSOR=6
      ELSE
       LUSOR=20
       OPEN(20,FILE=NOM,STATUS='UNKNOWN')
      ENDIF
10    WRITE(*,101)
      READ(*,*,ERR=10) KOPT
      IF(KOPT.EQ.0) GOTO 11
      IF(KOPT.EQ.1) CALL EDI3C
      IF(KOPT.EQ.2) CALL EDI6C
      IF(KOPT.EQ.3) CALL EDIG
      IF(KOPT.EQ.4) CALL EDIK
      IF(KOPT.EQ.5) CALL EDIRO
      IF(KOPT.EQ.6) CALL EDI3J
      IF(KOPT.EQ.7) GOTO 7
      GO TO 10
7     CONTINUE
      IF(LUSOR.EQ.20) CLOSE(20)
      END
C
      BLOCK DATA
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      CHARACTER KC*2,IIM*2,IUG*2
      COMMON /RL1/ LUSOR,IS(5)
      COMMON /RL2/ KC(5),IIM(3),IUG(2)
      COMMON /SY1/ DC(5),PC(5)
      DATA KC     /'A1','A2','E ','F1','F2'/
      DATA IIM    /'  ','  ',' i'/
      DATA IUG    /' g',' u'/
      DATA IS     /1,1,2,3,3/
      DATA DC     /1.,1.,2.,3.,3./
      DATA PC     /1.,-1.,1.,-1.,1./
      END
C-----------------------------------------------------------------------
C
      SUBROUTINE EDI3C
C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INTEGER CTR
      CHARACTER KC*2,IIM*2,IUG*2
      COMMON /RL1/ LUSOR,IS(5)
      COMMON /RL2/ KC(5),IIM(3),IUG(2)
C
1000  FORMAT(1X)
1010  FORMAT(/,'3C-sigma coefficients',
     &      //,' C1 C2 C3  s1 s2 s3   F')
1020  FORMAT(3(1X,A),1X,3I3,F17.13)
1030  FORMAT(/,I4,' 3C coefficients.')
C
      WRITE(LUSOR,1010)
      NB=0
      DO 110 IC1=1,5
       WRITE(LUSOR,1000)
       IS1M=IS(IC1)
       DO 100 IC2=1,5
        IS2M=IS(IC2)
        DO 101 IC3=1,5
         IS3M=IS(IC3)
         ITRI=CTR(IC1,IC2,IC3)
         IF(ITRI.LE.0) GOTO 101
         DO 90 IS1=1,IS1M
          DO 91 IS2=1,IS2M
           DO 92 IS3=1,IS3M
            F=TROIC(IC1,IC2,IC3,IS1,IS2,IS3)
            IF(F.EQ.0) GOTO 92
            WRITE(LUSOR,1020) KC(IC1),KC(IC2),KC(IC3),
     &                        IS1,IS2,IS3,
     &                        F
            NB=NB+1
92         CONTINUE
91        CONTINUE
90       CONTINUE
101     CONTINUE
100    CONTINUE
110   CONTINUE
      WRITE(*,1030) NB
      RETURN
      END
C-----------------------------------------------------------------------
C
      SUBROUTINE EDI6C
C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INTEGER CTR
      CHARACTER KC*2,IIM*2,IUG*2
      COMMON /RL1/ LUSOR,IS(5)
      COMMON /RL2/ KC(5),IIM(3),IUG(2)
C
1000  FORMAT(1X)
1010  FORMAT(/,'6C coefficients',
     &      //,' C1 C2 C3 C4 C5 C6   6C')
1020  FORMAT(6(1X,A),F17.13)
1030  FORMAT(/,I4,' 6C coefficients.')
C
      WRITE(LUSOR,1010)
      NB=0
      DO 180 IC1=1,5
       DO 181 IC2=1,5
        DO 182 IC3=1,5
         ITR1=CTR(IC1,IC2,IC3)
         IF(ITR1.LE.0) GOTO 182
         WRITE(LUSOR,1000)
         DO 170 IC4=1,5
          DO 171 IC5=1,5
           ITR2=CTR(IC4,IC5,IC3)
           IF(ITR2.LE.0) GOTO 171
           DO 160 IC6=1,5
            ITR34=CTR(IC4,IC2,IC6)*CTR(IC1,IC5,IC6)
            IF(ITR34.LE.0) GOTO 160
            S=SXC(IC1,IC2,IC3,IC4,IC5,IC6)
            WRITE(LUSOR,1020)
     &            KC(IC1),KC(IC2),KC(IC3),KC(IC4),KC(IC5),KC(IC6),
     &            S
            NB=NB+1
160        CONTINUE
171       CONTINUE
170      CONTINUE
182     CONTINUE
181    CONTINUE
180   CONTINUE
      WRITE(*,1030) NB
      RETURN
      END
C-----------------------------------------------------------------------
C
      SUBROUTINE EDIG
C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'TDS_PARAMETER'
      CHARACTER KC*2,IIM*2,IUG*2
      COMMON /RL1/ LUSOR,IS(5)
      COMMON /RL2/ KC(5),IIM(3),IUG(2)
C
1000  FORMAT(1X)
1010  FORMAT(/,'G matrix elements (Jmin=',I3,', Jmax=',I3,')',
     &      //,'   J    M   N  C s   G')
1020  FORMAT(I4,I5,I4,1X,A,I2,F19.15,A)
1030  FORMAT(/,'Please enter Jmin,Jmax: ')
1040  FORMAT(/,I8,' G matrix element(s).')
C
200   WRITE(*,1030)
      READ(*,*,ERR=201) JMIN,JMAX
      IF(JMIN.LT.0)    GOTO 201
      IF(JMAX.GT.MXJG) GOTO 201
      IF(JMIN.GT.JMAX) GOTO 201
      GOTO 202
201   WRITE(*,*) 'Invalid parameters!'
      GOTO 200
202   JMIP1=JMIN+1
      JMAP1=JMAX+1
      WRITE(LUSOR,1010) JMIN,JMAX
      NB=0
      DO 50 JP1=JMIP1,JMAP1
       J=JP1-1
       DO 51 IC=1,5
        ISM=IS(IC)
        NN=NSYM1(J,1,IC)
        IF(NN.LE.0) GOTO 51
        WRITE(LUSOR,1000)
        DO 40 NP1=1,NN
         N=NP1-1
         DO 41 ISC=1,ISM
          MMAX=MMAXS(J,IC,ISC)
          MMA=2*MMAX+1
          IPM=IPACS(IC,ISC)
          DO 42 MM=1,MMA,IPM
           M=MM-MMAX-1
           CALL GJCMS(J,M,N,IC,ISC,G,IP)
           IP=IP+2
           WRITE(LUSOR,1020) J,M,N,KC(IC),ISC,G,IIM(IP)
           NB=NB+1
42        CONTINUE
41       CONTINUE
40      CONTINUE
51     CONTINUE
50    CONTINUE
      WRITE(*,1040) NB
      RETURN
      END
C-----------------------------------------------------------------------
C
      SUBROUTINE EDIK
C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'TDS_PARAMETER'
      CHARACTER KC*2,IIM*2,IUG*2
      COMMON /RL1/ LUSOR,IS(5)
      COMMON /RL2/ KC(5),IIM(3),IUG(2)
C
1000  FORMAT(1X)
1010  FORMAT(/,'K isoscalar factors (J1=',I3,', parity=',I1,
     &         ', J2min=',I3,', J2max=',I3,', delta J3=',I3,')',
     &      //,'  J1    J2    J3   n1 C1 n2 C2 n3 C3   K')
1020  FORMAT(3(I4,A),3(I3,1X,A),F17.13,A)
1030  FORMAT(/,'Please enter J1,parity,J2min,J2max,delta J3: ')
1040  FORMAT(/,I8,' K isoscalar factor(s).')
1050  FORMAT('!!! J1+J2+J3 too large !!!')
C
200   WRITE(*,1030)
      READ(*,*,ERR=201) J1,IT,JMIN,JMAX,JDIF
      IF(J1.LT.0 .OR. J1.GT.MXGAM)    GOTO 201
      IF(IT.NE.1 .AND. IT.NE.2)       GOTO 201
      IF(JMIN.LT.0 .OR. JMIN.GT.MXJG) GOTO 201
      IF(JMAX.LT.0 .OR. JMAX.GT.MXJG) GOTO 201
      IF(JMIN.GT.JMAX)                GOTO 201
      IF(JDIF.LT.0 .OR. JDIF.GT.MXJG) GOTO 201
      GOTO 202
201   WRITE(*,*) 'Invalid parameters!'
      GOTO 200
202   JMIP1=JMIN+1
      JMAP1=JMAX+1
      NDJ=JDIF*2+1
      WRITE(LUSOR,1010) J1,IT,JMIN,JMAX,JDIF
      NB=0
      DO 170 IC1=1,5
       NN1=NSYM1(J1,IT,IC1)
       IF(NN1.LE.0) GOTO 170
       DO 160 N1P1=1,NN1
        WRITE(LUSOR,1000)
        N1=N1P1-1
        DO 150 J2P1=JMIP1,JMAP1
         J2=J2P1-1
         DO 151 JD=1,NDJ
          J3=J2+JD-JDIF-1
          IF(J3.LT.0 .OR. J3.GT.MXJG) GOTO 151
          IF(J1+J2+J3 .GT. (2*MXJG+MXGAM+1)) THEN
           WRITE(*,1050)
           GOTO 99
          ENDIF
          DO 140 IC2=1,5
           NN2=NSYM1(J2,1,IC2)
           IF(NN2.LE.0) GOTO 140
           DO 130 N2P1=1,NN2
            N2=N2P1-1
            DO 131 IC3=1,5
             NN3=NSYM1(J3,IT,IC3)
             IF(NN3.LE.0) GOTO 131
             DO 120 N3P1=1,NN3
              N3=N3P1-1
              CALL KCUBU(J1,J2,J3,IT,1,IT,N1,N2,N3,IC1,IC2,IC3,
     &                   AK,IK)
              IF(AK.EQ.0) GOTO 120
              IK=IK+2
              WRITE(LUSOR,1020) J1,IUG(IT),J2,IUG(1),J3,IUG(IT),
     &                          N1,KC(IC1),N2,KC(IC2),N3,KC(IC3),
     &                          AK,IIM(IK)
              NB=NB+1
120          CONTINUE
131         CONTINUE
130        CONTINUE
140       CONTINUE
151      CONTINUE
150     CONTINUE
160    CONTINUE
170   CONTINUE
      WRITE(*,1040) NB
99    RETURN
      END
C-----------------------------------------------------------------------
C
      SUBROUTINE EDIRO
C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'TDS_PARAMETER'
      CHARACTER KC*2,IIM*2,IUG*2
      COMMON /RL1/ LUSOR,IS(5)
      COMMON /RL2/ KC(5),IIM(3),IUG(2)
C
1000  FORMAT(1X)
1010  FORMAT(/,'Rotational reduced matrix elements (Omega max=',I1,
     &         ', Jmax=',I3,')',
     &      //,'<J||Omega(K)||J> =  r.m.e.')
1020  FORMAT('<',I3,'||',I1,'(',I1,')||',I3,'> =',E14.7)
1030  FORMAT(/,'Please enter Omega max,Jmax: ')
1040  FORMAT(/,I5,' reduced matrix element(s).')
C
200   WRITE(*,1030)
      READ(*,*,ERR=201) IMAX,JMAX
      IF(IMAX.LT.0 .OR. IMAX.GT.MXGAM)  GOTO 201
      IF(JMAX.LT.0 .OR. JMAX.GT.MXJG)   GOTO 201
      GOTO 202
201   WRITE(*,*) 'Invalid parameters!'
      GOTO 200
202   JMAP1=JMAX+1
      IMAP1=IMAX+1
      WRITE(LUSOR,1010) IMAX,JMAX
      NB=0
      DO 10 IMEP1=1,IMAP1
       IMEGA=IMEP1-1
       IPAP1=IMEGA-(IMEGA/2)*2+1
       DO 11 KP1=IPAP1,IMEP1,2
        WRITE(LUSOR,1000)
        K=KP1-1
        DO 12 JP1=1,JMAP1
         J=JP1-1
         E=EMRRO(IMEGA,K,J)
         NB=NB+1
         WRITE(LUSOR,1020) J,IMEGA,K,J,E
12      CONTINUE
11     CONTINUE
10    CONTINUE
      WRITE(*,1040) NB
      RETURN
      END
C-----------------------------------------------------------------------
C
      SUBROUTINE EDI3J
C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'TDS_PARAMETER'
      CHARACTER KC*2,IIM*2,IUG*2
      COMMON /RL1/ LUSOR,IS(5)
      COMMON /RL2/ KC(5),IIM(3),IUG(2)
C
1000  FORMAT(1X)
1010  FORMAT(/,'3J-m coefficients (J1=',I3,', J2min=',I3,
     &         ', J2max=',I3,', delta J3=',I3,')',
     &       /,'                               _',
     &       /,'  J1  J2  J3    m1   m2   m3   V')
1020  FORMAT(3I4,1X,3I5,F17.13)
1030  FORMAT(/,'Please enter J1,J2min,J2max,delta J3: ')
1040  FORMAT(/,I8,' 3J-m coefficient(s).')
1050  FORMAT('!!! J1+J2+J3 too large !!!')
C
200   WRITE(*,1030)
      READ(*,*,ERR=201) J1,JMIN,JMAX,JDIF
      IF(J1.LT.0 .OR. J1.GT.MXGAM)    GOTO 201
      IF(JMIN.LT.0 .OR. JMIN.GT.MXJG) GOTO 201
      IF(JMAX.LT.0 .OR. JMAX.GT.MXJG) GOTO 201
      IF(JMIN.GT.JMAX)                GOTO 201
      IF(JDIF.LT.0 .OR. JDIF.GT.MXJG) GOTO 201
      GOTO 202
201   WRITE(*,*) 'Invalid parameters!'
      GOTO 200
202   JMIP1=JMIN+1
      JMAP1=JMAX+1
      NDJ=JDIF*2+1
      WRITE(LUSOR,1010) J1,JMIN,JMAX,JDIF
      NB=0
      DO 170 IM1=-J1,J1
       WRITE(LUSOR,1000)
       DO 150 J2P1=JMIP1,JMAP1
        J2=J2P1-1
        DO 151 JD=1,NDJ
         J3=J2+JD-JDIF-1
         IF(J3.LT.0 .OR. J3.GT.MXJG) GOTO 151
         IF(J1+J2+J3.GT. (2*MXJG+MXGAM+1) ) THEN
          WRITE(*,1050)
          GOTO 99
         ENDIF
         DO 140 IM2=-J2,J2
          IM3=-IM1-IM2
          V=TROIJ(J1,J2,J3,IM1,IM2,IM3)
          IF(V.EQ.0) GOTO 140
          WRITE(LUSOR,1020) J1,J2,J3,IM1,IM2,IM3,V
          NB=NB+1
140      CONTINUE
151     CONTINUE
150    CONTINUE
170   CONTINUE
      WRITE(*,1040) NB
99    RETURN
      END

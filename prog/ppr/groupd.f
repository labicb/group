      PROGRAM GROUPD
C
C  22.9.88 FORTRAN 77 POUR SUN4
C
C  lecture complete des G : MXJG
C
C  EDITION DE COEFFICIENTS DE COUPLAGE
C
C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'TDS_PARAMETER'
      CHARACTER NOM*40
      CHARACTER KC*3,IIM*2,IUG*2
      COMMON /RL1/ LUSOR,IS(8)
      COMMON /RL2/ KC(8),IIM(3),IUG(2)
C
101   FORMAT(/,'0: Change name of output file.',
     &       /,'1: 3C-sigma coefficients.',
     &       /,'2: 6C coefficients.',
     &       /,'3: G matrix elements.',
     &       /,'4: K isoscalar factors.',
     &       /,'5: Rotational reduced matrix elements.',
     &       /,'6: 3J-m coefficients.',
     &       /,'7: Exit.',
     &      //,'Choose one of the items above:')
102   FORMAT(A)
103   FORMAT
     &  (/,' GROUPD Program.',
     &  //,'Please read file group_README for detailed instructions.',
     &   /,'Initializing, please wait.')
104   FORMAT('Loading G : integer ...')
105   FORMAT(/,'Type name of output file (ENTER = screen): ')
106   FORMAT('Loading G : half-integer ...')
C
C-----------------------------------------------------------------------
C
      WRITE(*,103)
      WRITE(*,104)
      CALL LECTG(MXJG)
      WRITE(*,106)
      CALL LECTGD
      CALL FACTO
      CALL LSIXCD
      CALL LTRCD
      CALL CAL6C
C
C-----------------------------------------------------------------------
C
11    WRITE(*,105)
      READ(*,102) NOM
      IF(NOM.EQ.' ') THEN
       LUSOR=6
      ELSE
       LUSOR=20
       OPEN(20,FILE=NOM,STATUS='UNKNOWN')
      ENDIF
10    WRITE(*,101)
      READ(*,*,ERR=10) KOPT
      IF(KOPT.EQ.0) GOTO 11
      IF(KOPT.EQ.1) CALL EDI3C
      IF(KOPT.EQ.2) CALL EDI6C
      IF(KOPT.EQ.3) CALL EDIG
      IF(KOPT.EQ.4) CALL EDIK
      IF(KOPT.EQ.5) CALL EDIRO
      IF(KOPT.EQ.6) CALL EDI3J
      IF(KOPT.EQ.7) GOTO 7
      GO TO 10
7     CONTINUE
      IF(LUSOR.EQ.20) CLOSE(20)
      END
C
      BLOCK DATA
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      CHARACTER KC*3,IIM*2,IUG*2
      COMMON /RL1/ LUSOR,IS(8)
      COMMON /RL2/ KC(8),IIM(3),IUG(2)
      COMMON /SY1/ DC(8),PC(5)
      DATA KC     /'A1','A2','E ','F1','F2','E''1','E''2','G'''/
      DATA IIM    /'  ','  ',' i'/
      DATA IUG    /' g',' u'/
      DATA IS     /1,1,2,3,3,2,2,4/
      DATA DC     /1.,1.,2.,3.,3.,2.,2.,4./
      DATA PC     /1.,-1.,1.,-1.,1./
      END
C-----------------------------------------------------------------------
C
      SUBROUTINE EDI3C
C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INTEGER CTR
      CHARACTER KC*3,IIM*2,IUG*2,BETA(2)*1,IJM(2)*2
      COMMON /RL1/ LUSOR,IS(8)
      COMMON /RL2/ KC(8),IIM(3),IUG(2)
      DATA IJM    /'  ',' i'/
C
1000  FORMAT(1X)
1010  FORMAT(/,'3C-sigma coefficients',
     &      //,' C1  C2  C3   Beta s1 s2 s3      F')
1020  FORMAT(3(1X,A),3X,A,2X,3I3,3X,F17.13,A)
1030  FORMAT(/,I4,' 3C coefficients.')
C
      WRITE(LUSOR,1010)
      NB=0
      DO 110 IC1=1,8
       WRITE(LUSOR,1000)
       IS1M=IS(IC1)
       DO 100 IC2=1,8
        IS2M=IS(IC2)
        DO 101 IC3=1,8
         IS3M=IS(IC3)
         ITRI=CTR(IC1,IC2,IC3)
         IF(ITRI.LE.0) GOTO 101
         DO 90 IS1=1,IS1M
          DO 91 IS2=1,IS2M
           DO 92 IS3=1,IS3M
            IC123=100*IC1+10*IC2+IC3
            IBMAX=1
            BETA(1)='-'
            IF((IC123.EQ.488).OR.
     &         (IC123.EQ.848).OR.
     &         (IC123.EQ.884)) THEN
             IBMAX=2
             BETA(1)='1'
             BETA(2)='3'
            ENDIF
            IF((IC123.EQ.588).OR.
     &         (IC123.EQ.858).OR.
     &         (IC123.EQ.885)) THEN
             IBMAX=2
             BETA(1)='a'
             BETA(2)='s'
            ENDIF
            DO 93 IB=1,IBMAX
             CALL TROICD(IC1,IC2,IC3,IB,IS1,IS2,IS3,F,IM)
             IF(F.EQ.0) GOTO 93
             WRITE(LUSOR,1020) KC(IC1),KC(IC2),KC(IC3),BETA(IB),
     &                         IS1,IS2,IS3,
     &                         F,IJM(IM+1)
             NB=NB+1
93          CONTINUE
92         CONTINUE
91        CONTINUE
90       CONTINUE
101     CONTINUE
100    CONTINUE
110   CONTINUE
      WRITE(*,1030) NB
      RETURN
      END
C-----------------------------------------------------------------------
C
      SUBROUTINE EDI6C
C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INTEGER CTR
      CHARACTER KC*3,IIM*2,IUG*2
      CHARACTER*1 BETA1(2),BETA2(2),BETA3(2),BETA4(2)
      COMMON /RL1/ LUSOR,IS(8)
      COMMON /RL2/ KC(8),IIM(3),IUG(2)
C
1000  FORMAT(1X)
1010  FORMAT(/,'6C coefficients',
     &      //,' C1  C2  C3  C4  C5  C6     Beta       6C')
1020  FORMAT(6(1X,A),2X,4(1X,A),2X,F17.13)
1030  FORMAT(/,I4,' 6C coefficients.')
C
      WRITE(LUSOR,1010)
      NB=0
C
C----BOUCLE C1
C
      DO 180 IC1=1,8
C
C----BOUCLE C2
C
       DO 181 IC2=1,8
C
C----BOUCLE C3
C
        DO 182 IC3=1,8
         ITR1=CTR(IC1,IC2,IC3)
         IF(ITR1.LE.0) GOTO 182
         IC123=100*IC1+10*IC2+IC3
         IBMAX4=1
         BETA4(1)='-'
         IF((IC123.EQ.488).OR.
     &      (IC123.EQ.848).OR.
     &      (IC123.EQ.884)) THEN
          IBMAX4=2
          BETA4(1)='1'
          BETA4(2)='3'
         ENDIF
         IF((IC123.EQ.588).OR.
     &      (IC123.EQ.858).OR.
     &      (IC123.EQ.885)) THEN
          IBMAX4=2
          BETA4(1)='a'
          BETA4(2)='s'
         ENDIF
         WRITE(LUSOR,1000)
C
C----BOUCLE C4
C
         DO 170 IC4=1,8
C
C----BOUCLE C5
C
          DO 171 IC5=1,8
           ITR2=CTR(IC4,IC5,IC3)
           IF(ITR2.LE.0) GOTO 171
           IC453=100*IC4+10*IC5+IC3
           IBMAX3=1
           BETA3(1)='-'
           IF((IC453.EQ.488).OR.
     &        (IC453.EQ.848).OR.
     &        (IC453.EQ.884)) THEN
            IBMAX3=2
            BETA3(1)='1'
            BETA3(2)='3'
           ENDIF
           IF((IC453.EQ.588).OR.
     &        (IC453.EQ.858).OR.
     &        (IC453.EQ.885)) THEN
            IBMAX3=2
            BETA3(1)='a'
            BETA3(2)='s'
           ENDIF
C
C----BOUCLE C6
C
           DO 160 IC6=1,8
            ITR34=CTR(IC4,IC2,IC6)*CTR(IC1,IC5,IC6)
            IF(ITR34.LE.0) GOTO 160
            IC426=100*IC4+10*IC2+IC6
            IBMAX2=1
            BETA2(1)='-'
            IF((IC426.EQ.488).OR.
     &         (IC426.EQ.848).OR.
     &         (IC426.EQ.884)) THEN
             IBMAX2=2
             BETA2(1)='1'
             BETA2(2)='3'
            ENDIF
            IF((IC426.EQ.588).OR.
     &         (IC426.EQ.858).OR.
     &         (IC426.EQ.885)) THEN
             IBMAX2=2
             BETA2(1)='a'
             BETA2(2)='s'
            ENDIF
            IC156=100*IC1+10*IC5+IC6
            IBMAX1=1
            BETA1(1)='-'
            IF((IC156.EQ.488).OR.
     &         (IC156.EQ.848).OR.
     &         (IC156.EQ.884)) THEN
             IBMAX1=2
             BETA1(1)='1'
             BETA1(2)='3'
            ENDIF
            IF((IC156.EQ.588).OR.
     &         (IC156.EQ.858).OR.
     &         (IC156.EQ.885)) THEN
             IBMAX1=2
             BETA1(1)='a'
             BETA1(2)='s'
            ENDIF
C
C ------------BOUCLES BETAS
C
            DO 141 IB1=1,IBMAX1
             DO 142 IB2=1,IBMAX2
              DO 143 IB3=1,IBMAX3
               DO 140 IB4=1,IBMAX4
                S=CAL6CD(IC1,IC2,IC3,IC4,IC5,IC6,IB1,IB2,IB3,IB4)
                IF(ABS(S).LT.1E-10) GOTO 140
                WRITE(LUSOR,1020)
     &                KC(IC1),KC(IC2),KC(IC3),KC(IC4),KC(IC5),KC(IC6),
     &                BETA1(IB1),BETA2(IB2),BETA3(IB3),BETA4(IB4),
     &                S
                NB=NB+1
140            CONTINUE
143           CONTINUE
142          CONTINUE
141         CONTINUE
160        CONTINUE
171       CONTINUE
170      CONTINUE
182     CONTINUE
181    CONTINUE
180   CONTINUE
      WRITE(*,1030) NB
      RETURN
      END
C-----------------------------------------------------------------------
C
      SUBROUTINE EDIG
C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'TDS_PARAMETER'
      DIMENSION NT(8)
      CHARACTER KC*3,IIM*2,IUG*2
      COMMON /RL1/ LUSOR,IS(8)
      COMMON /RL2/ KC(8),IIM(3),IUG(2)
C
1000  FORMAT(1X)
1010  FORMAT(/,'G matrix elements (Jmin=',F6.1,', Jmax=',F6.1,')',
     &      //,'    J      M    N  C  s   G')
1020  FORMAT(F6.1,1X,F6.1,I4,1X,A,I2,F19.15)
1030  FORMAT(/,'Please enter Jmin,Jmax: ')
1040  FORMAT(/,I8,' G matrix element(s).')
C
200   WRITE(*,1030)
      READ(*,*,ERR=201) AJMIN,AJMAX
      IF(INT(AJMIN).EQ.AJMIN .OR.
     &   INT(AJMIN*2.D0).NE.(AJMIN*2.D0)) GOTO 201
      IF(INT(AJMAX).EQ.AJMAX .OR.
     &   INT(AJMAX*2.D0).NE.(AJMAX*2.D0)) GOTO 201
      IF(AJMIN.LT.0.5D0)                  GOTO 201
      IF(AJMAX.GT.(DBLE(MXJG)+0.5D0))     GOTO 201
      IF(AJMIN.GT.AJMAX)                  GOTO 201
      GOTO 202
201   WRITE(*,*) 'Invalid parameters!'
      GOTO 200
202   WRITE(LUSOR,1010) AJMIN,AJMAX
      NB=0
      DO 50 AJP1=AJMIN,AJMAX
       AJ=AJP1
       J=INT(2.D0*AJ)
       CALL MULTCJ(J,NT)
       DO 51 IC=6,8
        IF(NT(IC).EQ.0) GOTO 51
        NN=NT(IC)
        ISM=IS(IC)
        WRITE(LUSOR,1000)
        DO 40 NP1=1,NN
         N=NP1-1
         DO 41 ISC=1,ISM
          CALL MDNBD(AJ,IC,ISC,MPAS,AMMIN,AMMAX,MNB)
          DO 1 MD=NINT(2.D0*AMMIN),NINT(2.D0*AMMAX),NINT(2.D0*MPAS)
           CALL GJCMSD(AJ,MD,N,IC,ISC,G)
           WRITE(LUSOR,1020) AJ,DBLE(MD)/2.D0,N,KC(IC),ISC,G
           NB=NB+1
1         CONTINUE
41       CONTINUE
40      CONTINUE
51     CONTINUE
50    CONTINUE
      WRITE(*,1040) NB
      RETURN
      END
C-----------------------------------------------------------------------
C
      SUBROUTINE EDIK
C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'TDS_PARAMETER'
      PARAMETER (MDEV=7)
      DIMENSION ICEV1(MDEV),ICEV3(MDEV),NMEV1(MDEV),NMEV3(MDEV)
      DIMENSION NT1(8),NT2(8),NT3(8),AK(2),IPP(2)
      CHARACTER KC*3,IIM*2,IUG*2,IJM(2)*2,BETA(2)*1
      COMMON /RL1/ LUSOR,IS(8)
      COMMON /RL2/ KC(8),IIM(3),IUG(2)
      DATA IJM    /'  ',' i'/
C
1000  FORMAT(1X)
1010  FORMAT
     &(/,'K isoscalar factors (J1=',F6.1,', parity=',A,
     & ', J2min=',F6.1,', J2max=',F6.1,', delta J3=',F6.1,')',
     &//,'    J1      J2      J3    n1 C1  n2 C2  n3 C3   Beta    K')
1020  FORMAT(3(F6.1,A),1X,3(I3,1X,A),3X,A,3X,F17.13,A)
1030  FORMAT(/,'Please enter J1,parity,J2min,J2max,delta J3: ')
1040  FORMAT(/,I8,' K isoscalar factor(s).')
1050  FORMAT('!!! J1+J2+J3 too large !!!')
C
200   WRITE(*,1030)
      READ(*,*,ERR=201) IAJ1,IT,AJMIN,AJMAX,IAJDIF
      IF(IAJ1.LT.0 .OR. IAJ1.GT.MXGAM)    GOTO 201
      IF(IT.NE.1 .AND. IT.NE.2)           GOTO 201
      IF(INT(AJMIN).EQ.AJMIN .OR.
     &   INT(AJMIN*2.D0).NE.(AJMIN*2.D0)) GOTO 201
      IF(INT(AJMAX).EQ.AJMAX .OR.
     &   INT(AJMAX*2.D0).NE.(AJMAX*2.D0)) GOTO 201
      IF(AJMIN.LT.0.5D0)                  GOTO 201
      IF(AJMAX.GT.(DBLE(MXJG)+0.5D0))     GOTO 201
      IF(AJMIN.GT.AJMAX)                  GOTO 201
      IF(IAJDIF.LT.0 .OR. IAJDIF.GT.MXJG) GOTO 201
      GOTO 202
201   WRITE(*,*) 'Invalid parameters!'
      GOTO 200
202   J1=2*IAJ1
      AJ1=DBLE(IAJ1)
      AJDIF=DBLE(IAJDIF)
      AJM=AJMIN+1.D0
      AJP=AJMAX+1.D0
      ANDJ=2.D0*AJDIF+1.D0
      WRITE(LUSOR,1010) AJ1,IUG(IT),AJMIN,AJMAX,AJDIF
      NB=0
C
      CALL MULTCJ(J1,NT1)
      DO 51 IC1=1,8
       IF(NT1(IC1).EQ.0) GOTO 51
       NN1=NT1(IC1)
       DO 61 N11=1,NN1
        N1=N11-1
        WRITE(LUSOR,1000)
        IC11=IC1
        CALL MULOS(IT,IC11,NEV1,ICEV1,NMEV1,MDEV)
        ICC1=ICEV1(1)
C
        DO 43 AAJ2=AJM,AJP
         AJ2=AAJ2-1.D0
         J2=INT(2.D0*AJ2)
         DO 44 AJD=1.D0,ANDJ
          AJ3=AJ2+AJD-AJDIF-1.D0
C
          IF((MOD(INT(2.D0*AJ3),2).EQ.1).AND.
     &       (AJ3.LT.0.5D0))                     GOTO 44
          IF((MOD(INT(2.D0*AJ3),2).EQ.1).AND.
     &       (AJ3.GT.DBLE(MXJG)+0.5))            GOTO 44
          IF(AJ3.LT.0.5D0 .OR. AJ3.GT.(DBLE(MXJG)+0.5D0)) GOTO 44
          IF(INT(AJ1+AJ2+AJ3).GT.(2*MXJG+MXGAM+3)) THEN
           WRITE(*,1050)
           RETURN
          ENDIF
C
          CALL MULTCJ(J2,NT2)
          DO 52 IC2=1,8
           IF(NT2(IC2).EQ.0) GOTO 52
           NN2=NT2(IC2)
           DO 62 N22=1,NN2
            N2=N22-1
            ICC2=IC2
            J3=INT(2.D0*AJ3)
            CALL MULTCJ(J3,NT3)
            DO 54 IC3=1,8
             IF(NT3(IC3).EQ.0) GOTO 54
             NN3=NT3(IC3)
             DO 63 N33=1,NN3
              N3=N33-1
              IC33=IC3
              CALL MULOS(IT,IC33,NEV3,ICEV3,NMEV3,MDEV)
              ICC3=ICEV3(1)
              IBMAX=1
              BETA(1)='-'
              IC123=100*ICC1+10*ICC2+ICC3
              IF((IC123.EQ.488).OR.
     &           (IC123.EQ.848).OR.
     &           (IC123.EQ.884)) THEN
               IBMAX=2
               BETA(1)='1'
               BETA(2)='3'
              ENDIF
              IF((IC123.EQ.588).OR.
     &           (IC123.EQ.858).OR.
     &           (IC123.EQ.885)) THEN
               IBMAX=2
               BETA(1)='a'
               BETA(2)='s'
              ENDIF
              CALL KCUBUD(J1,IT,J2,1,J3,IT,IC1,IC2,IC3,N1,N2,N3,
     &                    IBMAX,AK1,AK2,IP1,IP2)
              AK(1)=AK1
              AK(2)=AK2
              IPP(1)=IP1+1
              IPP(2)=IP2+1
              DO 45 I=1,IBMAX
               IF(ABS(AK(I)).LT.1E-10) GOTO 45
               WRITE(LUSOR,1020) AJ1,IUG(IT),AJ2,IUG(1),AJ3,IUG(IT),
     &                           N1,KC(ICC1),N2,KC(ICC2),N3,KC(ICC3),
     &                           BETA(I),AK(I),IJM(IPP(I))
               NB=NB+1
45            CONTINUE
63           CONTINUE
54          CONTINUE
62         CONTINUE
52        CONTINUE
44       CONTINUE
43      CONTINUE
61     CONTINUE
51    CONTINUE
      WRITE(*,1040) NB
99    RETURN
      END
C-----------------------------------------------------------------------
C
      SUBROUTINE EDIRO
C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'TDS_PARAMETER'
      CHARACTER KC*3,IIM*2,IUG*2
      COMMON /RL1/ LUSOR,IS(8)
      COMMON /RL2/ KC(8),IIM(3),IUG(2)
C
1000  FORMAT(1X)
1010  FORMAT(/,'Rotational reduced matrix elements (Omega max=',I1,
     &         ', Jmax=',F6.1,')',
     &      //,'<  J||Omega(K)||J  > =  r.m.e.')
1020  FORMAT('<',F5.1,'||',I1,'(',I1,')||',F5.1,'> =',E14.7)
1030  FORMAT(/,'Please enter Omega max,Jmax: ')
1040  FORMAT(/,I5,' reduced matrix element(s).')
C
200   WRITE(*,1030)
      READ(*,*,ERR=201) IMAX,AJMAX
      IF(IMAX.LT.0 .OR. IMAX.GT.MXGAM)                    GOTO 201
      IF(INT(AJMAX).EQ.AJMAX .OR.
     &   INT(AJMAX*2.D0).NE.(AJMAX*2.D0))                 GOTO 201
      IF(AJMAX.LT.0.5D0 .OR. AJMAX.GT.(DBLE(MXJG)+0.5D0)) GOTO 201
      GOTO 202
201   WRITE(*,*) 'Invalid parameters!'
      GOTO 200
202   IMAP1=IMAX+1
      WRITE(LUSOR,1010) IMAX,AJMAX
      NB=0
      DO 10 IMEP1=1,IMAP1
       IMEGA=IMEP1-1
       IPAP1=IMEGA-(IMEGA/2)*2+1
       DO 11 KP1=IPAP1,IMEP1,2
        WRITE(LUSOR,1000)
        K=KP1-1
        DO 12 AJP1=0.5D0,AJMAX
         OJ=AJP1
         OOJ=AJP1
         T1=(-4.D0*OJ*(OJ+1.D0))/SQRT(3.D0)
         T2=(T1)**((IMEGA-K)/2)
         E=T2*EMRROD(K,OOJ)
         NB=NB+1
         WRITE(LUSOR,1020) AJP1,IMEGA,K,AJP1,E
12      CONTINUE
11     CONTINUE
10    CONTINUE
      WRITE(*,1040) NB
      RETURN
      END
C-----------------------------------------------------------------------
C
      SUBROUTINE EDI3J
C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'TDS_PARAMETER'
      CHARACTER KC*3,IIM*2,IUG*2
      COMMON /RL1/ LUSOR,IS(8)
      COMMON /RL2/ KC(8),IIM(3),IUG(2)
C
1000  FORMAT(1X)
1010  FORMAT(/,'3J-m coefficients (J1=',F6.1,', J2min=',F6.1,
     &         ', J2max=',F6.1,', delta J3=',F6.1,')',
     &       /,'                                           _',
     &       /,'    J1    J2    J3      m1     m2     m3   V')
1020  FORMAT(3F6.1,1X,3F7.1,F17.13)
1030  FORMAT(/,'Please enter J1,J2min,J2max,delta J3: ')
1040  FORMAT(/,I8,' 3J-m coefficient(s).')
1050  FORMAT('!!! J1+J2+J3 too large !!!')
C
200   WRITE(*,1030)
      READ(*,*,ERR=201) IAJ1,AJMIN,AJMAX,IAJDIF
      IF(IAJ1.LT.0 .OR. IAJ1.GT.MXGAM)    GOTO 201
      IF(INT(AJMIN).EQ.AJMIN .OR.
     &   INT(AJMIN*2.D0).NE.(AJMIN*2.D0)) GOTO 201
      IF(INT(AJMAX).EQ.AJMAX .OR.
     &   INT(AJMAX*2.D0).NE.(AJMAX*2.D0)) GOTO 201
      IF(AJMIN.LT.0.5D0)                  GOTO 201
      IF(AJMAX.GT.(DBLE(MXJG)+0.5D0))     GOTO 201
      IF(AJMIN.GT.AJMAX)                  GOTO 201
      IF(IAJDIF.LT.0 .OR. IAJDIF.GT.MXJG) GOTO 201
      GOTO 202
201   WRITE(*,*) 'Invalid parameters!'
      GOTO 200
202   AJ1=DBLE(IAJ1)
      AJDIF=DBLE(IAJDIF)
      AJM=AJMIN+1.D0
      AJP=AJMAX+1.D0
      ANDJ=2.D0*AJDIF+1.D0
      WRITE(LUSOR,1010) AJ1,AJMIN,AJMAX,AJDIF
      NB=0
      DO 1 AM1=-AJ1,AJ1
       WRITE(LUSOR,1000)
       DO 10 AJJ2=AJM,AJP
        AJ2=AJJ2-1.D0
        DO 11 AJD=1.D0,ANDJ
         AJ3=AJ2+AJD-AJDIF-1.D0
C
         IF((MOD(INT(2.D0*AJ3),2).EQ.1).AND.
     &      (AJ3.LT.0.5D0))                       GOTO 11
         IF((MOD(INT(2.D0*AJ3),2).EQ.1).AND.
     &      (AJ3.GT.DBLE(MXJG)+0.5))              GOTO 11
         IF((AJ3.LT.0.5D0).OR.(AJ3.GT.(DBLE(MXJG)+0.5D0))) GOTO 11
         IF(INT(AJ1+AJ2+AJ3).GT.(2*MXJG+MXGAM+3)) THEN
          WRITE(*,1050)
          RETURN
         ENDIF
C
         DO 2 AM2=-AJ2,AJ2
          DO 4 AM3=-AJ3,AJ3
           V=TROIJD(AJ1,AJ2,AJ3,AM1,AM2,AM3)
           IF(ABS(V).LT.1E-10) GOTO 4
           WRITE(LUSOR,1020) AJ1,AJ2,AJ3,AM1,AM2,AM3,V
           NB=NB+1
4         CONTINUE
2        CONTINUE
11      CONTINUE
10     CONTINUE
1     CONTINUE
      WRITE(*,1040) NB
99    RETURN
      END

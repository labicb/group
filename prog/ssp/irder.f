C
C *** CREATION D'UN TABLEAU D'ORDONNANCEMENT.
C     LE TABLEAU ARGUMENT EST ENTIER
C
C SMIL C. MILAN MAI 75
C
      SUBROUTINE IRDER(M,K1,IND,MDM)
      DIMENSION K1(MDM),IND(MDM)
C
      N=1
      IND(1)=1
      IF(M-1)16,16,1
1     IT1=N+1
      IK=K1(IT1)
      J=IND(1)
      IF(IK-K1(J))2,2,11
11    I1=1
      I2=IT1
3     I=(I1+I2)/2
      J=IND(I)
      IF(IK-K1(J))12,4,4
12    I2=I
      GO TO 5
4     I1=I
5     IF(I2-I1-1)3,6,3
2     I2=1
6     IF(I2-IT1)13,7,13
13    IR=N
8     IT2=IR+1
      IND(IT2)=IND(IR)
      IF(I2-IR)14,7,14
14    IR=IR-1
      GO TO 8
7     IND(I2)=IT1
      IF(M-IT1)15,16,15
15    N=N+1
      GO TO 1
16    RETURN
      END

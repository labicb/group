C
C *** PERMUTTE LES LIGNES/COLONNES D'UN TABLEAU ENTIER SUIVANT L'ORDRE
C     CROISSANT DES ELEMENTS DE LA PREMIERE LIGNE/COLONNE DU TABLEAU
C
C SMIL C. MILAN MAI 75
C
      SUBROUTINE RENGE(L,N,M,IPER)
      DIMENSION L(N,M)
C
      IPER=1
      NM1=N-1
      DO 1 I=1,NM1
        K=0
        LI=L(I,1)
        IP1=I+1
        DO 2 J=IP1,N
          IF(L(J,1) .GE. LI) GO TO 2
          K=J
          LI=L(K,1)
2       CONTINUE
        IF(K.EQ.0) GO TO 1
        DO 3 J=1,M
          LL=L(I,J)
          L(I,J)=L(K,J)
3         L(K,J)=LL
        IPER=-IPER
1     CONTINUE
      RETURN
      END

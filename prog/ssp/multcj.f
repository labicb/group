C
C    ********* TABLE DE SUBDUCTION SU(2) -- O^S ********
C
C
      SUBROUTINE MULTCJ(IR,N)
C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION NENT(12),NDEMI(12),N(8)
      DATA NENT / 10000,10,101,1011,10111,121,11112,1122,10222,11132
     &,11223,1233 /
      DATA NDEMI / 100,1,11,111,102,112,122,113,213,223,124,224 /
C
      EPSIL=1.D-8
      A=DBLE(IR)/2.D0
      TEST=DABS(A-DBLE(NINT(A)))
      IF(TEST.LE.EPSIL) THEN
        ITEST=1
      ELSE
        ITEST=2
      ENDIF
      GOTO(10,20)ITEST
C
C     --- CAS J ENTIER
C
10    IA=NINT(A)
      IP=IA/12
      IR1=IA-12*(IA/12)+1
      NB=NENT(IR1)
C
      N(1)=NB/10000
      N(2)=(NB-N(1)*10000)/1000
      N(3)=(NB-N(1)*10000-N(2)*1000)/100
      N(4)=(NB-N(1)*10000-N(2)*1000-N(3)*100)/10
      N(5)=NB-N(1)*10000-N(2)*1000-N(3)*100-N(4)*10
      N(6)=0
      N(7)=0
      N(8)=0
      N(1)=N(1)+IP
      N(2)=N(2)+IP
      N(3)=N(3)+2*IP
      N(4)=N(4)+3*IP
      N(5)=N(5)+3*IP
      RETURN
C
C     --- CAS J DEMI-ENTIER
C
20    N(1)=0
      N(2)=0
      N(3)=0
      N(4)=0
      N(5)=0
      IA=NINT(A-.5D0)
      IP=IA/12
      IR1=IA-12*(IA/12)+1
      NB=NDEMI(IR1)
      N(6)=NB/100
      N(7)=(NB-N(6)*100)/10
      N(8)=NB-N(6)*100-N(7)*10
      N(6)=N(6)+2*IP
      N(7)=N(7)+2*IP
      N(8)=N(8)+4*IP
      RETURN
      END

C
C *** SYMBOLES G DE SO(3)
C
C
      SUBROUTINE GJCMSD(OJJ,MM,NN,ICC,ISS,G)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'TDS_PARAMETER'
      COMMON /LGD/ GJD(MXGD),IND(MXJG+2),ICODD(MXGD)
C
      G=0.D0
      OJ=OJJ
      M=MM
      N=NN
      IC=ICC
      IS=ISS
C
      IPHASE=ABS(M)/M
C       iphase=isign(m,1)
      J1=NINT(OJ)
      J2=NINT(OJ+1.D0)
      I1=IND(J1)
      I2=IND(J2)-1
C
C  --- CODAGE
C
      ICO=(10000*ABS(M)+100*NN+10*IC+IS)*IPHASE
C
C --- TRI DICHOTOMIE
C
      IF(ICO.LT.ICODD(I1)) RETURN
      IF(ICO.GT.ICODD(I2)) RETURN
      IF(ICO.EQ.ICODD(I1)) THEN
         G=GJD(I1)
         RETURN
      ENDIF
      IF(ICO.EQ.ICODD(I2)) THEN
         G=GJD(I2)
         RETURN
      ENDIF
1     I=(I1+I2)/2
C
      IF(ICO-ICODD(I)) 2,3,4
2     IF((I2-I1).EQ.1) RETURN
      I2=I
      GOTO 1
4     IF((I2-I1).EQ.1) RETURN
      I1=I
      GOTO 1
3     G=GJD(I)
      RETURN
      END

C
C *** SYMBOLES "3J" CUBIQUES.
C
C SMIL CHAMPION DEC 78
C
      SUBROUTINE FCUBU(J1,J2,J3,IU1,IU2,IU3,N1,N2,N3,ICC1,ICC2,ICC3,
     &                 II1,II2,II3,F,I)
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INTEGER NTR,MMAXS,IPACS
      DOUBLE PRECISION TROIJ
C
      IG1 = 0                                                           INITIALISATION PAR DEFAUT
      IG2 = 0                                                           INITIALISATION PAR DEFAUT
      IG3 = 0                                                           INITIALISATION PAR DEFAUT
C
      IU=IU1+IU2+IU3
      IF(IU-(IU/2)*2)10,9,10
10    NT=NTR(J1,J2,J3)
      F=0.D0
      I=0
      IF(NT)1,9,1
9     RETURN
1     IC1=ICC1
      IC2=ICC2
      IC3=ICC3
      S1=1.D0
      S2=1.D0
      S3=1.D0
      I1=II1
      I2=II2
      I3=II3
      IF(IU1-2)101,100,101
100   CALL UG(ICC1,II1,IC1,I1,S1)
101   IF(IU2-2)103,102,103
102   CALL UG(ICC2,II2,IC2,I2,S2)
103   IF(IU3-2)105,104,105
104   CALL UG(ICC3,II3,IC3,I3,S3)
105   MA1=MMAXS(J1,IC1,I1)
      MA2=MMAXS(J2,IC2,I2)
      MA3=MMAXS(J3,IC3,I3)
      M21=MA1*2+1
      M22=MA2*2+1
      IP1=IPACS(IC1,I1)
      IP2=IPACS(IC2,I2)
      DO 3 IM1=1,M21,IP1
        M1=IM1-MA1-1
        CALL GJCMS(J1,M1,N1,IC1,I1,GJ1,IGG1)
        IF(GJ1.NE.0.0D0)IG1=IGG1
        DO 3 IM2=1,M22,IP2
          M2=IM2-MA2-1
          CALL GJCMS(J2,M2,N2,IC2,I2,GJ2,IGG2)
          IF(GJ2.NE.0.0D0)IG2=IGG2
          M3=-M1-M2
          IF(IABS(M3)-MA3)30,30,3
30        CALL GJCMS(J3,M3,N3,IC3,I3,GJ3,IGG3)
          IF(GJ3.NE.0.0D0)IG3=IGG3
          F=F+GJ1*GJ2*GJ3*TROIJ(J1,J2,J3,M1,M2,M3)
3     CONTINUE
      F=F*S1*S2*S3
      IG=IG1+IG2
      IF(IG-1)5,5,4
4     IG=0
      F=-F
5     IF(IG+IG3-1)6,7,8
6     RETURN
7     I=1
      RETURN
8     F=-F
      RETURN
      END
